import request from './components/request'


class FormController{

    constructor(form, params={}) {
        this.form = form
        this.table = document.getElementById('table-container')
        this.errors = document.getElementById('errors-container')
        this.ok = document.getElementById('ok-container')
        this.initEvents()
    }

    initEvents() {
        this.form.addEventListener('submit', event => {
            event.preventDefault()
            this.send()
        })
    }

    send() {
        let data = new FormData(this.form)
        let url = this.form.action
        let method = this.form.method

        this.errors.innerHTML = ''
        this.ok.innerHTML = ''

        request(method, url, data)
            .then(response => {
                switch (response.success){
                    case true:
                        this.table.innerHTML = response.template
                        this.ok.innerHTML = response.message
                        break;

                    case false:
                        this.errors.innerHTML = JSON.stringify(response.errors)
                        break;

                    default:
                        break;
                }
                return response
            })

    }
}


const form = document.getElementById('mainForm')
const formControl = new FormController(form)