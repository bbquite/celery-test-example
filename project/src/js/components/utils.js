export class OpenOtherController{
    /**
     * Контроллер для открывания и сворачивания скрытых пунктов списка
     * @param {HTMLElement} block
     */

    constructor(block) {
        this.block = block
        this.trigger = block.querySelector('.js-open-other-trigger')
        this.hiddenList = block.querySelectorAll('js-hidden')
        if (this.trigger != undefined && this.hiddenList != undefined)
            this.initEvents()
    }

    initEvents() {
        this.trigger.addEventListener('click', event => {
            this.toggleList()
        })
    }

    toggleList() {
        if (this.block.classList.contains('opened'))
            this.hideOther()
        else
            this.openOther()
    }

    hideOther() {
        this.block.classList.remove('opened')
        this.trigger.innerHTML = '<span>Все категории</span>'
            
    }

    openOther() {
        this.block.classList.add('opened')
        this.trigger.innerHTML = '<span>Скрыть категории</span>'
    }
}

/**
  * Делит число на разряды разделителем
  * @param x - число
  * @param del - разделитель
  * @returns {string}
  */
export function formatNumber(x, del) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, del || " ");
}


/** 
 * Проверка устройства 
 */
export function isMobile(){return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);}


/**
 * Проверка ширины скрола
 * @returns int ширина скрола
 */
export function getScrollBarWidth () {
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";
  
    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild (inner);
  
    document.body.appendChild (outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;
  
    document.body.removeChild (outer);
  
    return (w1 - w2);
};