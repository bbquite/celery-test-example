import time
import openpyxl

from django.utils import timezone
from celery import shared_task

from .models import FileModel


@shared_task
def processing_xlsx(model_object_id):

    model_object = FileModel.objects.get(pk=model_object_id)
    file_path = model_object.file_obj.url

    reader = ExcelReader(file_path)
    result = reader.processing()

    model_object.result = result
    model_object.finished_at = timezone.now()
    model_object.save()

    print('task finished')
    return True


class ExcelReader:
    """
    BIG_FILE - иммитация большого файла
    (10 секунд задержки перед ответом) для проверки очереди
    """

    BIG_FILE = True

    def __init__(self, file_path) -> None:
        self.file_path = file_path
        self.workbook = openpyxl.load_workbook(
            filename=f'../{self.file_path}')

        self.before_info = {}
        self.after_info = {}

        self.is_found = False

    def processing(self):
        """
        Запуск обработки
        """
        result = 'Содержимое файла не соответствует условиям'

        self.find_columns()

        if self.is_found:
            before_values = self.get_column_values(self.before_info)
            after_values = self.get_column_values(self.after_info)

            result = self.find_result(before_values, after_values)

        if self.BIG_FILE:
            time.sleep(10)
        
        return result

    def find_columns(self):
        """
        Поиск колонок по файлу
        после нахождения обоих колонок цикл завершается
        """

        for sheet in self.workbook.sheetnames:
            if self.before_info and self.after_info:
                self.is_found = True
                break

            sheet_obj = self.workbook[sheet]

            for x in range(1, sheet_obj.max_column+1):
                if self.before_info and self.after_info:
                    break
                column_title = sheet_obj.cell(row=1, column=x).value

                if column_title == 'before':
                    self.before_info.update({
                        'sheet': sheet_obj,
                        'column': x
                    })
                
                if column_title == 'after':
                    self.after_info.update({
                        'sheet': sheet_obj,
                        'column': x
                    })

    def get_column_values(self, info: dict) -> list:
        """
        Формируем список значений ячеек колонки
        """

        sheet = info['sheet']
        column = info['column']
        result = []
        for x in range(2, sheet.max_row+1):
            value = sheet.cell(row=x, column=column).value
            if value is not None:
                result.append(int(value))
        
        return result

    def find_result(self, before, after):
        result_str = ''
        before.sort()
        after.sort()

        # print('before', before)
        # print('after', after)

        if len(before) > len(after):
            L2 = before
            result_str = 'removed:'
        else:
            L2 = after
            result_str = 'added:'

        # print(f'before\t - \tafter')

        for num in range(len(L2)):
            try:
                # print(f'{before[num]}\t - \t{after[num]}')
                if not before[num] == after[num]:
                    result_str = f'{result_str} {L2[num]}'
                    break
            except IndexError:
                result_str = f'{result_str} {L2[num]}'

        # print(result_str)

        return result_str
