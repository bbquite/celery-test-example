import os

from django.db import models


class FileModel(models.Model):

    class StatusesChoice(models.TextChoices):
        LOAD = 'LOAD', 'Загружено'
        PROCESSING = 'PROCESSING', 'Обрабатывается'
        READY = 'READY', 'Обработано'

    status = models.CharField(
        max_length=127, choices=StatusesChoice.choices,
        default=StatusesChoice.LOAD)
    file_obj = models.FileField(
        verbose_name='Файл', blank=False)
    created_at = models.DateTimeField(
        verbose_name='Дата загрузки', auto_now_add=True)
    finished_at = models.DateTimeField(
        verbose_name='Дата завершения', blank=True, null=True)
    result = models.TextField(
        verbose_name='Результат', blank=True)
    
    def __str__(self) -> str:
        return f'Файл {self.pk} {self.created_at}'

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
        ordering = ('-created_at',)

    def get_info(self):
        name, extension = os.path.splitext(self.file_obj.name)
        return name, extension[1:]

    def get_size(self):
        size = os.path.getsize(self.file_obj.path)
        if size < 512000:
            size = size / 1024.0
            ext = 'Кб'
        elif size < 4194304000:
            size = size / 1048576.0
            ext = 'Мб'
        else:
            size = size / 1073741824.0
            ext = 'Гб'
        return f'{size:.2f} {ext}'
