import os
from django import forms
from .models import FileModel


class FileModelForm(forms.ModelForm):
    ALLOWED_TYPES = ['xlsx']

    file_error_messages = {'required': 'Загрузите файл'}

    file_obj = forms.FileField(error_messages=file_error_messages)

    class Meta:
        model = FileModel
        fields = ('file_obj',)

    def clean_file_obj(self):
        file_obj = self.cleaned_data.get('file_obj', None)
        if not file_obj:
            raise forms.ValidationError('Файл не загружен')
   
        extension = os.path.splitext(file_obj.name)[1][1:].lower()
        if extension in self.ALLOWED_TYPES:
            return file_obj
        else:
            raise forms.ValidationError('Тип файла не разрешен')
 