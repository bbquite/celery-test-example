from django.dispatch import receiver
from django.db import transaction
from django.db.models.signals import post_save

from .tasks import processing_xlsx
from .models import FileModel


@receiver(post_save, sender=FileModel)
def new_file_callback(sender, instance, created, **kwargs):
    if created:
        transaction.on_commit(
            lambda:processing_xlsx.delay(instance.pk))