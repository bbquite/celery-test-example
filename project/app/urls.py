from django.urls import path
from .views import IndexView, ProcessingView

app_name = 'app'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('api/processing/', ProcessingView.as_view(), name='processing'),
]
