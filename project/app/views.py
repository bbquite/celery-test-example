from django.views.generic import TemplateView, CreateView
from django.http.response import JsonResponse
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.db import transaction

from .forms import FileModelForm
from .models import FileModel


class IndexView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'files_qs': FileModel.objects.all()})
        return context


class AjaxableResponseMixin:
    http_method_names = ['post']

    def dispatch(self, request, *args, **kwargs):
        token = request.POST.get('token', None)
        if token and token == '5806d76c-2728-4dc1-a3e7-e79f374b6b95':
            return super().dispatch(request, *args, **kwargs)
        return HttpResponse(status=403)

    def form_invalid(self, form):
        return JsonResponse({
            'errors': form.errors,
            'success': False
        })

    def form_valid(self, form):
        data = {'success': True}
        data.update(self.extra_data())
        return JsonResponse(data)

    def extra_data(self):
        return {}


class ProcessingView(AjaxableResponseMixin, CreateView):
    table_template = 'table.html'
    form_class = FileModelForm

    @transaction.atomic()
    def form_valid(self, form):
        self.object = form.save()
        return super().form_valid(form)
    
    def extra_data(self):
        return {
            'message': 'Файл отправлен на обработку',
            'template': render_to_string(
                self.table_template, {
                    'files_qs': FileModel.objects.all()
                }
            )
        }