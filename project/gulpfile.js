// Base
const path              = require('path');
const gulp              = require('gulp');

// Server
const browsersync 		= require('browser-sync').create();

// General
const sourcemaps        = require('gulp-sourcemaps');

// Scripts
const webpack           = require('webpack')
const webpackStream     = require('webpack-stream');

// Styles
const sass              = require('gulp-sass');
const autoprefixer      = require('gulp-autoprefixer');
const cleanCss 			= require('gulp-clean-css')

// Images
const tinypng 			= require('gulp-tinypng-compress');
const svgSprite 		= require('gulp-svg-sprite');

// webp
const webp = require('gulp-webp');


// Paths
const paths = {
    build:  path.resolve(__dirname, 'static/js/'),
    node:   path.join(__dirname, 'node_modules'),
    src: {
        self:       path.join(__dirname, 'src'),
        js:         './src/js/',
        sass:       './src/scss/',
        images:     './src/images/',
        pug:        './src/jade/',
    },
    static: {
        self:       './static/',
        js:         './static/js/',
        css:        './static/css/',
        images:     './static/images/',
	},
    html: './'
}


let webpackConfig = {
    // mode: "production",
    mode: "development",
    entry: {
		index: path.join(paths.src.self, "js", "index.js"),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      presets: ['@babel/preset-env'],
                      plugins: [
						  ['@babel/plugin-proposal-class-properties'],
						  ['@babel/plugin-transform-runtime'],
						  ['@babel/plugin-syntax-dynamic-import'],
					  ]
                    }
                }
            },
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx"],
        modules: [
            paths.node,
            paths.src.self,
        ]
    },
    output: {
        path: paths.build,
        publicPath: "/static/js/",
		filename: "[name].bundle.js",
		chunkFilename: '[name].bundle.js',
        library: "[name]"
	},

	optimization: {
		minimize: true,
	},

	plugins: [
		// new webpack.ProvidePlugin({
		// 	$: "jquery",
		// 	jQuery: "jquery",
		// })
	]
}

function jsTask() {
	return gulp
		.src(paths.src.js + "*.js")
		.pipe(webpackStream(webpackConfig))
		.pipe(gulp.dest(paths.build))
		.pipe(browsersync.stream())
}
gulp.task(jsTask);


function jsBuildTask() {
	let config = webpackConfig
	config.mode = "production"

	return gulp
		.src(paths.src.js + "*.js")
		.pipe(webpackStream(config))
		.pipe(gulp.dest(paths.build));
}
gulp.task(jsBuildTask);


function sassTask() {
	return gulp
		.src(paths.src.sass + "*.{scss,sass,css}")
		.pipe(sourcemaps.init())
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer({
			grid: "autoplace",
			browsers: ['last 2 versions']  
		}))
		.pipe(cleanCss({
			level: {
				1: {
					specialComments: "none"
				}
			}
		}))
		.pipe(sourcemaps.write("./", { sourceRoot: "/source/scss" }))
		.pipe(gulp.dest(paths.static.css))
		.pipe(browsersync.stream())
}
gulp.task(sassTask);


function sassBuildTask() {
	return gulp
		.src(paths.src.sass + "*.{scss,sass,css}")
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer({
			grid: "autoplace",
			browsers: ['last 2 versions']  
		}))
		.pipe(cleanCss({
			level: {
				1: {
					specialComments: "none"
				}
			}
		}))
		.pipe(gulp.dest(paths.static.css))
}
gulp.task(sassBuildTask);


function tinypngTask() {
	return gulp
		.src(paths.src.images + '*.{png,jpg,jpeg}')
		.pipe(tinypng({
			key: 'Gwq3s3RpzwPKW5xywCqBlGjdQKpgX2XP',
			sigFile: paths.static.images + '/.tinypng-sigs'
		}))
		.pipe(gulp.dest(paths.static.images))
}
gulp.task(tinypngTask);


function webpTask() {
	return gulp
		.src(paths.src.images + 'webp/*.{png,jpg,jpeg}')
		.pipe(webp())
		.pipe(gulp.dest(paths.static.images))
}
gulp.task(webpTask);

function svgTask() {
	return gulp.src(paths.src.images + '*.svg')
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: "../sprite.svg"
				}
			},
			svg: {
				doctypeDeclaration: false,
				xmlDeclaration: false
			},
			shape: {
				transform: [{
					svgo:{
						plugins: [
							{removeAttrs: {attrs: '(fill|stroke)'}},
							{removeXMLNS: true}
						]
					}
				}]
			}
		}))
		.pipe(gulp.dest(paths.static.images))
}
gulp.task(svgTask);


function staticSvgTask() {
	return gulp.src(paths.src.images + 'static/*.svg')
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: "../static-sprite.svg"
				}
			},
			svg: {
				doctypeDeclaration: false,
				xmlDeclaration: false
			},
			shape: {
				transform: [{
					svgo:{
						plugins: [
							{removeAttrs: {attrs: 'svg:(fill|stroke)'}},
							{removeXMLNS: true}
						]
					}
				}]
			}
		}))
		.pipe(gulp.dest(paths.static.images))
}
gulp.task(staticSvgTask);


function watchTask() {
	gulp.watch([paths.src.js + "**/*.js"], gulp.parallel(jsTask));
	gulp.watch([paths.src.sass + "**/*.scss"], gulp.parallel(sassTask));
	
	gulp.watch([paths.src.images + '*.{png,jpg,jpeg}'], gulp.parallel(tinypngTask));
	// gulp.watch([paths.src.images + 'webp/*.{png,jpg,jpeg}'], gulp.parallel(webpTask));
	gulp.watch([paths.src.images + '*.svg'], gulp.parallel(svgTask));
	gulp.watch([paths.src.images + 'static/*.svg'], gulp.parallel(staticSvgTask));
	return gulp
}
gulp.task(watchTask);

gulp.task('default', gulp.parallel(watchTask, jsTask, sassTask, tinypngTask, svgTask, staticSvgTask));
gulp.task('build', gulp.parallel(jsBuildTask, sassBuildTask));